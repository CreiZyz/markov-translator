#include <iostream>
#include <cstdlib>
#include <string.h>
#include <fstream>

bool testExtension(char *dico,const char *ext)
{
	using namespace std;
	
	char *extension = strstr(dico,".");

	if (extension == 0)
	{
		return 0;
	}
	else
	{
		return (strcmp(extension,ext) == 0);
	}
	
}


void afficherHelp()
{
	using namespace std;

	ifstream fichier("help.txt", ios::in);
	string contenu;
	while(getline(fichier, contenu))  
    {
    	cout << contenu << endl;	
    }
    
    fichier.close(); 
    
}


int main(int argc, char *argv[])
{
	using namespace std;
	char *dico = 0;
	char *graphe = 0;
	bool rules = false;
	bool erreur = false;
	string s;


	for (int i = 1; i < argc; ++i)
	{

		if (strcmp(argv[i],"--help") == 0)
		{

			afficherHelp();
			exit(1);
		}

		else if (strcmp(argv[i-1],"-o") != 0 && *argv[i] != '-')
		{
 			ifstream file(argv[i]);
			if (!file)
			{
				erreur = true;
				s = s + "erreur : " + argv[i] + " : Aucun fichier de ce type" + "\n";
				//cout << "erreur : " << argv[i] << " : Aucun fichier de ce type" << "\n";
			}
  			else
  			{
  				dico = argv[i];
  			}
  			file.close();
		}		
	
		else if (strcmp(argv[i],"-o") == 0)
		{
			graphe = argv[i+1];
		}	

		else if (strcmp(argv[i],"-rules=true") == 0)
		{
			bool rules = true;
		}

		else if (strcmp(argv[i-1],"-o") != 0 )
		{
			s = s + "erreur : option non reconnue " +  argv[i] + "\n";
			//cout << "erreur : option non reconnue "<<  argv[i] << "\n";
			erreur = true;
		}
	}

	if (dico == 0)
	{
		s = s + "erreur : pas de dictionnaire d'entree" + "\n";
		//cout  << "erreur : pas de dictionnaire d'entree" << "\n";
		erreur = true;


	}
	else if (testExtension(dico,".txt") == 0)
	{
		s = s + "erreur : extension de fichier faux (.txt pour le dictionnaire)" + "\n";
		//cout << "erreur : extension de fichier faux (.txt pour le dictionnaire)" << "\n";
		erreur = true;

	}

	if (graphe == 0)
	{
		s = s + "erreur : pas de graphe de sortie" + "\n";
		//cout  << "erreur : pas de graphe de sortie" << "\n";
		erreur = true;	
	}

	if (erreur = true)
	{
		cout << s << "\n";
		cout << "Saisissez « ./kesgen --help » pour plus d'informations." << "\n";

		exit(1);
	}
	else
	{

	}
 
	return 0;
}