Le 12.04.2014

---

Membre affectés:
 Simon Rapilly - Thomas Gillet

---

Travail effectué:
 Le programme kesgen est le programme qui sera chargé de générer un graphe (transducteur) probabiliste à partir
 d'un dictionnaire. Nous avons réalisé la partie qui précède cette génération: la partie qui gère les commandes
 utilisables par l'utilisateur pour paramétrer le programme. Nous avons codé: --help, -rules (option qui définie
 que la génération du graphe (transducteur) doit s'appuyer sur les règles de traductions FR - IPA pré-existantes),
 -o (pour définir le fichier dans lequel produire le graphe).

---

Travail qu'il reste à effectuer:
 Nous devons par la suite coder la partie du programme qui permettra de lancer l'analyse du dictionnaire. Par la
 suite, nous pouvons envisager que les choix que nous ferons (nouvelles options, etc...) crérons la nécessité de
 revenir sur le programme présent.

---

Fichiers présents:
 - help.txt : fichier d'aide qui est affiché lorsque l'utilisateur tape la commande "kesgen --help"
 - kesgen.cpp : notre programme
