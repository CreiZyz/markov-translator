Le 12.04.2014

---

Membre affectés:
 Emilien Masson, Julian Didier

---

Travail effectué:
 Nous avons produit un début de documentation en latex. Actuellement, nous avons fini de rédiger une
 annexe explicative sur les graphes. Nous avons commencé un lexique sur les graphes, et nous avons
 commencé à rédiger un rapport du projet, qui détaillera notre projet, les réflexions et choix que
 nous avons fait, ainsi que l'algorithme que nous avons mit au point. De plus, nous accompagnerons
 ce rapport d'un manuel d'utilisateur.

---

Travail qu'il reste à effectuer:
 Nous devons terminer la documentation, finir de rédiger le rapport, et de rédiger le manuel d'utilisation.

---

Fichier présents:
 - graphes.pdf : une annexe explicative sur les graphes
 - lexique_graphe.pdf : un lexique sur les termes attenants aux graphes
 - Rapport_intermédiaire : un rapport intermédiaire concernant notre projet

---
