#include <vector>
#include <string>
#include <iostream>

using namespace std;

class Node{
protected:
	// Valeur FR de note Node
	string my_FR_value;
	// Valeur IPA de notre Node
	string my_IPA_value;
	// Poid de notre Node
	double my_weight;
	// Nombre de successeurs
	int my_nbNextNodes;
	// lien vers les éléments suivants
	vector<Node*> *my_nextNodes;
	// lien vers l'élément précédent.
	Node *my_previousNode;
public:
	// Constructeur de la base d'un arbre
	Node():my_FR_value("A"),my_IPA_value("B"),my_nextNodes(NULL),my_previousNode(NULL){
		cout << "Creation du Node initial: " << this << endl;
		// Vector contenant des pointeurs vers les Nodes successeurs
		my_nextNodes = new vector<Node*>(1);
		// cout << "Creation du vecteur contenant les successeurs du Node: " << this << endl;
	}
	// Destructeur de node
	~Node(){
		// Si il existe des successeurs, il faut les détruire aussi
		if(my_nextNodes != NULL){
			// Desalocation de tous les successeurs
			for(int n=1; n<my_nbNextNodes; n++)	delete nextNode(n);
			// Desalocation du vecteur contenant les adresses des successeurs
			delete my_nextNodes;
		}
		cout << "Destruction du Node: " << this << endl;
	}
	Node* addNode(){
		// Ajoute un pointeur sur le successeur
		my_nextNodes->push_back(NULL);
		// Allocation mémoire pour le successeur en renseignant le parent.
		my_nextNodes->back() = new Node(this);
		// Compte le nombre de successeurs
		my_nbNextNodes += 1;
		// Renvoi un pointeur vers le nouveau Node.
		return my_nextNodes->back();
	}
	Node* nextNode(int n){
		// Retourne l'adresse du Node successeur numéro n.
		return (*my_nextNodes)[n];
	}
	vector<Node*>* nextNode(){
		// Retourne l'adresse du tableau de pointeur sur les successeurs
		return my_nextNodes;
	}
	Node* previousNode(){
		return my_previousNode;
	}
	string getFR(){
		return my_FR_value;
	}
	void setFR(string FR){
		my_FR_value = FR;
	}
	string getIPA(){
		return my_IPA_value;
	}
	void setIPA(string IPA){
		my_IPA_value = IPA;
	}
	void setFinalNode(){
		delete my_nextNodes;
		my_nextNodes = NULL;
	}
	void setWeight(double weight){
		my_weight = weight;
	}
	double getWeight(){
		return my_weight;
	}
	int getNbNextNodes(){
		return my_nbNextNodes;
	}
protected:
	// Constructeur d'un Node dans un abre
	Node(Node* previousNode):my_FR_value("a"),my_IPA_value("b"),my_nextNodes(NULL),my_previousNode(previousNode){
		cout << "Creation du Node " << this << " successeur de " << my_previousNode << "." << endl;
		// Vector contenant des pointeurs vers les Nodes successeurs
		my_nextNodes = new vector<Node*>(1);
		// cout << "Creation du vecteur contenant les successeurs du Node: " << this << endl;
	}
};