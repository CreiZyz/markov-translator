#include <iostream>
#include <string>
#include "parseur.cpp"
#include "node.cpp"

using namespace std;

class Analyseur{
protected:
	string my_dictionnaire;
	Node* my_arbreDeTraductions;

public:
	/**
	* Constructeur de la classe Analyseur
	* @param dictionnaire le chemin vers un dictionnaire
	*/
	Analyseur(string dictionnaire):my_dictionnaire(dictionnaire),my_arbreDeTraductions(NULL){

	}
	/**
	* Destructeur de la classe Analyseur
	* Le destructeur de la classe analyseur est chargé de détruire l'arbre de traduction en cas d'exception
	*/
	~Analyseur(){
		if(my_arbreDeTraductions != NULL) delete my_arbreDeTraductions;
	}
	/**
	* Analyse un dictionnaire et produit un graphe orienté pondéré à partir de celui ci
	* @param d l'emplacement du dictionnaire à analyser
	*/
	void analyserDictionnaire(){
		// On ouvre un flux sur le dictionnaire
		ifstream dictionnaire(my_dictionnaire.c_str());
		// On déclare un tableau qui servira à stocker une entrée du dictionnaire.
		// motDictionnaire[0] correspondra au mot Francais, et motDictionnaire[1] correspondra au mot IPA
		string motDictionnaire[2];
		// On déclare un arbre de traduction
		Node* my_arbreDeTraductions = NULL;
		// On crée un nouveau graphe openFst
		cout << "Analyseur - Création d'un graphe" << endl;
		// On lit chaque ligne du dictionnaire, et pour chaque ligne
		while(extraireLigne(&dictionnaire, motDictionnaire, 2, TABULATION)){
			cout << "Analyse de la traduction de: " << motDictionnaire[0] << endl;
			// On crée l'arbre des traductions possibles du mot extrait du dictionnaire à partir des règles de traductions
			my_arbreDeTraductions = creationArbreTraductionAvecRegles(motDictionnaire[0], motDictionnaire[1]);
			// On modifie le graphe openFst à partir de l'arbre des traductions possibles généré
			// On désaloue la mémoire allouée à l'abre des traductions
			delete my_arbreDeTraductions;
		}
		// On désaloue la mémoire allouée au flux sur le dictionnaire
		dictionnaire.close();
		// On sauvegarde le graphe
		cout << "Analyseur - Sauvegarde du graphe" << endl;
	}

protected:
	/**
	* Crée l'arbre de traduction d'un mot à partir d'une traduction du mot et des règles de traduction.
	* @param motFrancais le mot dont nous devons générer l'abre de traduction
	* @param motIpa la traduction du mot
	* @return Un pointeur sur l'arbre de traduction
	* L'arbre de traduction représente les motifs/patterns de conversion/traduction utilisés pour traduire le mot.
	*/
	Node* creationArbreTraductionAvecRegles(string motFrancais, string motIpa){
		// On ouvre un flux sur le fichier qui contient les règles de traduction Francais - Ipa
		ifstream reglesTraduction("regles.txt");
		// On initialise un arbre de traduction
		Node* traduction = new Node();
		// On lance la génération des noeuds de l'arbre de traduction de notre mot
		int nbTraductions = genererNoeudsTraduction(traduction, motFrancais, motIpa, &reglesTraduction);
		cout << "Nombre de traductions possibles: " << nbTraductions << endl;
		// On pondère l'arbre de traduction en fonction du nombre d'occurence des motifs qui le composent
		double poid = pondererArbreTraduction(traduction, nbTraductions);
		cout << poid << endl;
		// Lorsque la génération des noeuds est terminée, on renvoi l'abre de traduction
		return traduction;
	}

	/**
	* Génère les noeuds qui composent l'arbre de traduction d'un mot
	* @param arbreTraduction le noeud précédent
	* @param motFrancais le mot dont nous analysons la traduction
	* @param motIpa la traduction du mot
	* @param *reglesTraduction un pointeur vers le flux qui gère le fichier dans lequel sont recensées les règles de traduction
	* @param nbTraductions compteur du nombre de traductions possibles générées par notre méthode
	* @return le nombre de traductions possibles générées par notre méthode
	* La fonction genererNoeudsTraduction est une fonction récursive
	*/
	int genererNoeudsTraduction(Node* arbreTraduction,string motFrancais,string motIpa, ifstream* reglesTraduction, int nbTraductions = 0){
		// On part du principe que ce noeud sera un noeud final, le dernier motif d'une traduction possible.
		// Si on lui alloue des successeurs, alors il ne sera pas un noeud final.
		bool estUnNoeudFinal(true);
		// Pour vérifier toutes les règles de traduction, on se place au début du fichier de règles
		reglesTraduction->seekg(0, ios::beg);
		// On stockera les règles de traduction rencontrées dans le tableau ligneRegle
		// ligne[0] représentera la partie FR d'un motif. ligne[1] représentera la partie IPA d'un motif.
		string ligne[2];
		// Pour chaque ligne qui contient une règle dans le fichier, on lit la regle et on le compare
		// au début des mots analysés. Si un motif est trouvé dans un mot, on crée un noeud qui représente le motif de conversion
		while(extraireLigne(reglesTraduction, ligne, 2, TABULATION)){
			string partieFR_Regle(ligne[0]), partieIPA_Regle(ligne[1]);
			// Si le mot francais contient le motif de traduction
			if (partieFR_Regle.compare(motFrancais.substr(0,partieFR_Regle.size())) == 0){
				// Si le mot contient aussi la partie IPA du pattern
				if (partieIPA_Regle.compare(motIpa.substr(0,partieIPA_Regle.size())) == 0){
					// Le noeud n'est pas un noeud final
					estUnNoeudFinal = false;
					// On ajoute un noeud à la suite de celui ci.
					Node* newNode = arbreTraduction->addNode();
					// On remplie sa partie FR
					newNode->setFR(motFrancais.substr(0,partieFR_Regle.size()));
					// On remplie sa partie IPA
					newNode->setFR(motIpa.substr(0,partieIPA_Regle.size()));	
					// Nous allons maintenant générer tous les noeuds successeurs de celui que nous venons de créer.
					// Nous stockons notre position dans le fichier des règles de traduction pour pouvoir y revenir plus tard.
					// Nous sommes obligé de faire ca puisque le flux n'est pas ouvert dans la fonction, mais est l'un de ses paramêtres.
					int positionDansFichier = reglesTraduction->tellg();
					// On génère les noueuds successeurs du noeud que nous venons de créer en rappelant la fonction en utilisant seulement
					// Les parties de la chaine de caractère pour lesquelles nous n'avons pas créés de noeuds
					nbTraductions += genererNoeudsTraduction(newNode, motFrancais.substr(partieFR_Regle.size()), motIpa.substr(partieIPA_Regle.size()), reglesTraduction, nbTraductions);
					// Nous retournons à l'emplacement ou nous étions dans le fichier pour continuer de vérifier s'il n'existe pas d'autres règles applicables
					reglesTraduction->seekg(positionDansFichier);
				} 	
			} 
		}
		// Si le noeud est un noeud final, on désaloue le tableau dynamique qui lui était alloué.
		if(estUnNoeudFinal){
			// Appel de la méthode faite pour cela
			arbreTraduction->setFinalNode();
			// On en profite pour compter le nombre de traductions possibles générées. Cela servira a donner un poid aux noeuds de l'abre des traductions
			nbTraductions++;
		}
		// On retourne le nombre de traductions possibles générées
		return nbTraductions;
	}

	/**
	* Pondère un arbre de traduction en fonction du nombre d'occurence des motifs qui le composent
	* @param nodeActuel le node initial de notre arbre de traduction
	* @param nbTraductions le nombre de traductions possibles que représentent notre arbre
	*/
	double pondererArbreTraduction(Node* nodeActuel, int nbTraductions){
		// On initialise son poid à 0
		double poidNoeud = 0;
		// Un motif est représenté par un noeud
		// On vérifie si le noeud est un noeud final. Le poid d'un noeud final correspond à 1/nombre de traductions possibles
		// Le poid d'un noeud non final est égal à la somme des poids des noeuds qui le suivent.
		// On vérifie si nous sommes actuellement su un noeud final
		// Si non:
		if(nodeActuel->nextNode() != NULL){
			// cout << 1 << endl;
			// Le poid de notre noeud est la somme du poid de ses successeurs
			// Pour tous les successeurs
			for(vector<Node*>::iterator it = (nodeActuel->nextNode())->begin(); it != (nodeActuel->nextNode())->end(); ++it){
				// cout << 2 << endl;
				// On selectionne le successeur suivant
				Node* &successeur = *it;
				// cout << successeur << endl;
				// On calcul son poid
				double poidSuccesseur = pondererArbreTraduction(successeur, nbTraductions);
				// cout << 3 << endl;
				// On ajoute son poid au poid de notre noeud
				poidNoeud += poidSuccesseur;
			}
			// On inscrit le poid de notre noeud dans celui ci. Plus tard nous nous servirons de des
			// informations pour modifier le graphe openFst
		// Si il n'y a pas de successeurs
		} else {
			// Le poid du node est égal à 1/nombre de Translations possibles
			poidNoeud = 1/nbTraductions;
			cout << 4 << endl;
		}
		nodeActuel->setWeight(poidNoeud);
		return poidNoeud;
	}

	// Méthode de modification du graphe en fonction de l'arbre des Translations
	// void Analyseur::convertIntoGraphData(){

	// }
};