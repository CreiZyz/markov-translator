Le 12.04.2014

---

Membre affectés:
 Rémi Laot - Jordan Vidament - Simon Rapilly

---

Travail effectué:
 Nous découpons l'algorithme de génération du graphe (transducteur) en deux parties: pour chaque couple (mot, traduction)
 présent dans le dictionnaire, on génère les schémas de traduction possibles du mot (combinaisons de motifs de traductions.
 Exemple: (1 -> I), puis (2 -> II et III), puis (4 et 5 -> IV) avec 1,2,3,4,5 des caractères francais et I,II,III,IV des
 caractères phonétiques). Une fois ce schéma généré, on modifie le graphe (transducteur) en incorporant les motifs non encore
 pris en charge, et en corrigeant le poid dans le graphe de chaque motif rencontré dans les schémas de traduction. Nous prenons
 en charge l'ambiguité comme ceci: si un mot comporte plusieurs schémas de traduction (plusieurs suite d'application de règles
 possibles pour obtenir la traduction donnée), alors la probabilité de chaque motif d'être correct corresponde à 1 sur le
 nombre de traductions possibles. En effet, si un motif est rencontré dans tous les schémas de traductions générés, alors
 nous sommes sur qu'il s'agit du bon. On peut donc définir qu'il apparait une fois de plus dans le dictionnaire. Si au
 contraire la génération des schémas de traductions possibles nous proposent deux schémas schémas dont seuls les derniers
 motifs sont différents, alors nous pouvons partir du principe que chacun de ces deux motifs a 50% de chance d'être le
 motif utilisé pour produire la traduction donnée dans le dictionnaire. Ainsi, on peut partir du principe qu'ils
 apparaissent 0.5 fois de plus dans le dictionnaire.
 Au final, nous nous servirons du nombre d'occurence d'un motif de traduction dans le dictionnaire pour pondérer notre
 graphe.

---

Travail qu'il reste à effectuer:
 Nous avons actuellement un algorithme qui s'occupe de générer un schéma de traduction pour une entrée du dictionnaire.
 Cet algorithme n'est pour l'instant pas fonctionnel, en effet une erreur de segmentation apparait lorsque l'on essais
 de le lancer. Nous travaillons actuellement sur ce problème.
 En plus de la correction de l'algorithme déjà produit, nous devons encore produire tout l'algorithme qui récupère les
 informations contenues dans les schémas de traduction pour ensuite générer un graphe (transducteur) probabiliste exploitable.

---

Fichier présents:
 - test.cpp : programme de test de notre algorithme de génération de graphe.
 - parseur.cpp : code permettant de parser une ligne, en extractant de celle-ci un nombre n demandés de mots.
 - Analyseur.cpp : code de la classe Analyseur, qui s'occupe de l'analyse du dictionnaire et de la génération du graphe.
 - node.cpp : code de la structure de donnée qui nous permet de représenter les différents schémas de traduction d'un mot.

---
