#include <iostream>
#include <fstream>

using namespace std;

/**
 * @brief Liste les types de séparateurs pour la fonction extraire ligne
 * @details Deux mots peuvent être séparés par: une tabulation, un espace.
 */
enum Separateur {TABULATION, ESPACE};

/**
 * @brief Extrait une ligne d'un flux.
 * @param input le flux depuis lequel on extrait la ligne.
 * @param un pointeur vers le tableau de string dans lequel on extrait les mots de la ligne.
 * @param n le nombre de mots à lire sur la ligne.
 * @param separateur le type de séparateur utilisé pour parser les mots.
 * @return true si une ligne a été extraite, false si aucune ligne n'a été extraite.
 */
bool extraireLigne(ifstream* input, string* output, int n, Separateur separateur){
	// Déclaration des variables internes
	bool ligneExtraite(false);
	string ligne;
	// Si le flux n'est pas terminé
	if(input->good()){
		// On lit une ligne
		getline(*input, ligne);
		// Affichage de la ligne en cous de traitement
		// cout << "extraireLigne - extraction de " << n << " mots depuis la ligne: " << ligne << "." << endl;
		// On lit le nombre de mots demandés dans cette ligne
		for(int i=0; i<n; i++){
			// sep représente la taille du premier mot
			int sep(-1);
			// Les mots sont séparés pas des tabulations
			if(separateur == TABULATION){
				// On recherce une tabulation pour calculer la taille du premier mot
				sep = ligne.find("\t");}
			// Les mots sont séparés par des espaces
			else if(separateur == ESPACE){
				// On recherce un espace pour calculer la taille du premier mot
				sep = ligne.find(" ");}
			// On stocke le mot
			output[i] = ligne.substr(0, sep);
			// On le supprime de la ligne
			ligne = ligne.substr(sep+1);
			// Affichage des mots extraits
			// cout << "...... mot " << i+1 << " extrait: " << output[i] << "." << endl;
			// Si sep = -1, alors il n'y a qu'un seul mot sur la ligne
			if(sep == -1){
				// Affichage du message
				// cout << "... un seul mot sur la ligne." << endl;
				// On arrête la boucle
				i = n;
			}
		}
		// La ligne a été extraite
		ligneExtraite = true;
	}
	// On retourne l'état de la lecture
	return ligneExtraite;
}