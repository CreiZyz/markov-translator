#include <vector>
#include <string>
#include <iostream>

using namespace std;

/**
 * @class Noeud noeud.h "Noeud"
 * @brief Représente un noeud dans un abre.
 * @details Cette structure de donnée représente un noeud dans un
 *          arbre. Un Noeud n'a qu'un prédécesseur, mais peut av-
 *          oir plusieurs successeurs.
 *          Le lien vers le prédécesseur est un simple pointeur
 *          vers un Noeud. Les liens vers les successeurs sont eux
 *          contenus dans un conteneur de type vector<Noeud*>.
 * @author Rémi Laot
 * @version 0.1
 */
class Noeud{
protected:
	vector<Noeud*> listeSuccesseur;
	Noeud* predecesseur;

public:
	/**
	 * @brief Constructeur de Noeud
	 * @details Sert à initialiser un abre. Attention, les Noeuds
	 *          doivent être déclarés de façon dynamique, à l'aide
	 *          de l'opérateur new.
	 */
	Noeud():listeSuccesseur(0),predecesseur(NULL){
		// On réserve de la place pour 10 successeurs, au dela il faudra
		// faire une réalocation à chaque ajout d'un successeur
		listeSuccesseur.reserve(10);
		// cout << this << " - Creation" << endl;
	}

protected:
	/**
	 * @brief Constructeur de Noeud
	 * @details Ce constructeur ne doit en aucun cas être appelé par
	 *          l'utilisateur de la classe. Il est appelé par la mé-
	 *          thode ajouterSuccesseur().
	 *
	 * @param ptr_predecesseur Un pointeur sur le prédécesseur.
	 */
	Noeud(Noeud* ptr_predecesseur):listeSuccesseur(0),predecesseur(ptr_predecesseur){
		// On réserve de la place pour 10 successeurs, au dela il faudra
		// faire une réalocation à chaque ajout d'un successeur
		listeSuccesseur.reserve(10);
		// cout << this << " - Creation" << endl;
	}

public:
	/**
	 * @brief Destructeur de la classe Noeud
	 * @details Le destructeur est chargé de détruire les successeurs
	 *          du noeud.
	 */
	~Noeud(){
		for(vector<Noeud*>::iterator it = listeSuccesseur.begin(); it != listeSuccesseur.end(); ++it){
			Noeud* &successeur = *it;
			delete successeur;
		}
		// cout << this << " - Destruction" << endl;
	}

	/**
	 * @brief Fonction d'affichage d'un arbre
	 * @details Permet d'afficher tous les noeuds qui composent un abre
	 */
	void afficher(int indice = 0){
		cout << this << " - Noeud" << endl;
		for(vector<Noeud*>::iterator it = listeSuccesseur.begin(); it != listeSuccesseur.end(); ++it){
			Noeud* &successeur = *it;
			for(int i = 0; i <= indice; i++){
				cout << "\t - ";
			}
			successeur->afficher(indice+1);
		}
	}

	/**
	 * @brief Ajoute un successeur au Noeud
	 * @details Crée un nouveau Noeud successeur du Noeud sur lequel
	 *          est appliqué la méthode.
	 * 
	 * @return Un pointeur sur le successeur nouvellement créé.
	 */
	Noeud* ajouterSuccesseur(){
		listeSuccesseur.push_back(NULL);
		listeSuccesseur.back() = new Noeud(this);
		return listeSuccesseur.back();
	}

	/**
	 * @brief Permet d'accéder au successeur numéro n
	 * 
	 * @param n Le numéro du successeur auquel accéder
	 * @return Un pointeur sur le successeur 
	 */
	Noeud* get_successeur(int n){
		return listeSuccesseur.at(n);
	}

	/**
	 * @brief Permet d'accéder au conteneur des successeurs
	 * @return Un pointeur sur le conteneur des successeurs
	 */
	vector<Noeud*>* get_listeSuccesseur(){
		return &listeSuccesseur;
	}
};