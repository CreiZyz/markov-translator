#include <iostream>
#include <string>
#include "parseur.cpp"
#include "motif.cpp"

using namespace std;

class Analyseur{
protected:
	string my_dictionnaire;
	// inclure l'arbre de traductions dans les attributs de notre classe permet
	// de rendre son destructeur responsable de la désalocation de l'arbre. Cela
	// évite les fuites mémoires.
	Motif* my_arbreDeTraductions;

public:
	/**
	 * @brief Constructeur de la classe Analyseur
	 * @param dictionnaire le chemin vers un dictionnaire
	 */
	Analyseur(string dictionnaire):my_dictionnaire(dictionnaire),my_arbreDeTraductions(NULL){

	}

	/**
	 * @brief Destructeur de la classe Analyseur
	 * @details Le destructeur de la classe analyseur est chargé de détruire l'arbre de traduction en cas d'exception
	 */
	~Analyseur(){
		if(my_arbreDeTraductions != NULL) delete my_arbreDeTraductions;
	}

	/**
	 * @brief Analyse un dictionnaire et produit un graphe orienté pondéré à partir de celui ci
	 * 
	 * @param d l'emplacement du dictionnaire à analyser
	 */
	void analyserDictionnaire(){
		// On ouvre un flux sur le dictionnaire
		ifstream dictionnaire(my_dictionnaire.c_str());
		// On déclare un tableau qui servira à stocker une entrée du dictionnaire.
		// motDictionnaire[0] correspondra au mot Francais, et motDictionnaire[1] correspondra au mot IPA
		string motDictionnaire[2];
		// On crée un nouveau graphe openFst
		cout << "Analyseur - Création d'un graphe" << endl;
		// On lit chaque ligne du dictionnaire, et pour chaque ligne
		while(extraireLigne(&dictionnaire, motDictionnaire, 2, TABULATION)){
			cout << "Analyse de la traduction de: " << motDictionnaire[0] << endl;
			// On crée l'arbre des traductions possibles du mot extrait du dictionnaire à partir des règles de traductions
			my_arbreDeTraductions = creationArbreTraductionAvecRegles(motDictionnaire[0], motDictionnaire[1]);
			// On modifie le graphe openFst à partir de l'arbre des traductions possibles généré
			my_arbreDeTraductions->afficher();
			// On désaloue la mémoire allouée à l'abre des traductions
			delete my_arbreDeTraductions;
			my_arbreDeTraductions = NULL;
		}
		// On désaloue la mémoire allouée au flux sur le dictionnaire
		dictionnaire.close();
		// On sauvegarde le graphe
		cout << "Analyseur - Sauvegarde du graphe" << endl;
	}

protected:
	/**
	 * @brief Crée l'arbre de traduction d'un mot à partir d'une traduction du mot et des règles de traduction.
	 * @details  L'arbre de traduction représente les motifs/patterns de conversion/traduction utilisés pour traduire le mot.
	 * 
	 * @param motFrancais le mot dont nous devons générer l'abre de traduction
	 * @param motIpa la traduction du mot
	 * @return Un pointeur sur l'arbre de traduction
	 */
	Motif* creationArbreTraductionAvecRegles(string motFrancais, string motIpa){
		// On ouvre un flux sur le fichier qui contient les règles de traduction Francais - Ipa
		ifstream reglesTraduction("regles.txt");
		// On initialise un arbre de traduction
		Motif* traduction = new Motif();
		// On lance la génération des noeuds de l'arbre de traduction de notre mot
		int nbTraductions = genererNoeudsTraduction(traduction, motFrancais, motIpa, &reglesTraduction);
		cout << "Nombre de traductions possibles: " << nbTraductions << endl;
		// On pondère l'arbre de traduction en fonction du nombre d'occurence des motifs qui le composent
		pondererArbreTraduction(traduction, nbTraductions);
		// Lorsque la génération des noeuds est terminée, on renvoi l'abre de traduction
		return traduction;
	}

	/**
	 * @brief Génère les noeuds qui composent l'arbre de traduction d'un mot
	 * @details  La fonction genererNoeudsTraduction est une fonction récursive
	 * 
	 * @param arbreTraduction le noeud précédent
	 * @param motFrancais le mot dont nous analysons la traduction
	 * @param motIpa la traduction du mot
	 * @param *reglesTraduction un pointeur vers le flux qui gère le fichier dans lequel sont recensées les règles de traduction
	 * @param nbTraductions compteur du nombre de traductions possibles générées par notre méthode
	 * @return le nombre de traductions possibles générées par notre méthode
	 */
	int genererNoeudsTraduction(Motif* arbreTraduction,string motFrancais,string motIpa, ifstream* reglesTraduction, int nbTraductions = 0){
		// Pour vérifier toutes les règles de traduction, on se place au début du fichier de règles
		reglesTraduction->seekg(0, ios::beg);
		// On stockera les règles de traduction rencontrées dans le tableau ligneRegle
		// ligne[0] représentera la partie FR d'un motif. ligne[1] représentera la partie IPA d'un motif.
		string ligne[2];
		// Pour chaque ligne qui contient une règle dans le fichier, on lit la regle et on le compare
		// au début des mots analysés. Si un motif est trouvé dans un mot, on crée un noeud qui représente le motif de conversion
		while(extraireLigne(reglesTraduction, ligne, 2, TABULATION)){
			// On stocke les deux parties du motif trouvées dans deux variables de type
			// string pour faciliter la lecture de l'algorithme.	
			string partieFR_Regle(ligne[0]), partieIPA_Regle(ligne[1]);
			// Si le mot francais contient le motif de traduction
			// La méthode compare renvoi 0 si les deux strings sont équivalentes.
			if (partieFR_Regle.compare(motFrancais.substr(0,partieFR_Regle.size())) == 0){
				// Si le mot contient aussi la partie IPA du pattern
				if (partieIPA_Regle.compare(motIpa.substr(0,partieIPA_Regle.size())) == 0){
					// On ajoute un noeud à la suite de celui ci.
					Motif* nouveauMotif = arbreTraduction->ajouterSuccesseur();
					// On remplie sa partie FR
					nouveauMotif->set_partieFR(motFrancais.substr(0,partieFR_Regle.size()));
					// On remplie sa partie IPA
					nouveauMotif->set_partieIPA(motIpa.substr(0,partieIPA_Regle.size()));	
					// Nous allons maintenant générer tous les noeuds successeurs de celui que nous venons de créer.
					// Nous stockons notre position dans le fichier des règles de traduction pour pouvoir y revenir plus tard.
					// Nous sommes obligé de faire ca puisque le flux n'est pas ouvert dans la fonction, mais est l'un de ses paramêtres.
					int positionDansFichier = reglesTraduction->tellg();
					// On génère les noueuds successeurs du noeud que nous venons de créer en rappelant la fonction en utilisant seulement
					// Les parties de la chaine de caractère pour lesquelles nous n'avons pas créés de noeuds
					nbTraductions += genererNoeudsTraduction(nouveauMotif, motFrancais.substr(partieFR_Regle.size()), motIpa.substr(partieIPA_Regle.size()), reglesTraduction, nbTraductions);
					// Nous retournons à l'emplacement ou nous étions dans le fichier pour continuer de vérifier s'il n'existe pas d'autres règles applicables
					reglesTraduction->seekg(positionDansFichier);
				} 	
			} 
		}
		// Si le Motif n'a pas de successeurs, alors nous avons fini de calculer une traduction possible.
		// Nous calculons le nombre de traductions possibles trouvées par la méthode pour pouvoir par la
		// suite pondérer chaque motif pour prendre compte de l'ambiguité.
		if((arbreTraduction->get_listeSuccesseur())->size() == 0) nbTraductions++;
		// On retourne le nombre de traductions possibles générées
		return nbTraductions;
	}

	/**
	 * @brief Pondère un arbre de traduction en fonction du nombre d'occurence des motifs qui le composent
	 * 
	 * @param motifActuel le Motif initial de notre arbre de traduction
	 * @param nbTraductions le nombre de traductions possibles que représentent notre arbre
	 */
	double pondererArbreTraduction(Motif* motifActuel, int nbTraductions){
		// On initialise le poid du motif à 0
		double poidNoeud = 0;
		// On vérifie s'il existe des successeurs au motif. Si le motif a des successeurs,
		// son poid est égal à la somme du poid de tous les successeurs.
		if((motifActuel->get_listeSuccesseur())->size() != 0){
			// Le poid de notre noeud est la somme du poid de ses successeurs
			// On calcul le poid de tous les successeurs
			// Pour chaque successeur dans le conteneur de nos successeurs
			for(vector<Motif*>::iterator it = (motifActuel->get_listeSuccesseur())->begin(); it != (motifActuel->get_listeSuccesseur())->end(); ++it){
				// On selectionne le successeur suivant
				Motif* &successeur = *it;
				// cout << successeur << endl;
				// On calcul son poid
				double poidSuccesseur = pondererArbreTraduction(successeur, nbTraductions);
				// On ajoute son poid au poid de notre noeud
				poidNoeud += poidSuccesseur;
			}
		// Si il n'y a pas de successeurs
		} else {
			// Le poid du Motif est égal à 1/nombre de Translations possibles
			poidNoeud = 1/nbTraductions;
		}
		// On inscrit le poid de notre noeud dans celui ci. Plus tard nous nous servirons de des
		// informations pour modifier le graphe openFst
		motifActuel->set_poid(poidNoeud);
		return poidNoeud;
	}

	// Méthode de modification du graphe en fonction de l'arbre des Translations
	// void Analyseur::convertIntoGraphData(){

	// }
};