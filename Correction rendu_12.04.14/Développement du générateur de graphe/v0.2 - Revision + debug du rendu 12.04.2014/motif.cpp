#include <string>
#include <iostream>
#include <vector>

using namespace std;

/**
 * @class Motif motif.h "motif"
 * @brief Représente un Motif de traduction dans un arbre de traductions
 * @details Un Motif de traduction comporte une partie Francaise, et une partie
 *          IPA. Un motif représente le fait que la partie Francaise se traduise
 *          par la partie IPA. Un motif a un poid, qui permet de compter son n-
 *          ombre d'occurences.
 * 
 * @author Rémi Laot
 * @version 0.1
 */
class Motif{
protected:
	vector<Motif*> listeSuccesseur;
	Motif* predecesseur;
	string partieFR;
	string partieIPA;
	double poid;

public:
	/**
	 * @brief Constructeur de la classe Motif
	 * @details Constructeur initial d'un arbre de traduction
	 */
	 Motif():listeSuccesseur(0),predecesseur(NULL),partieFR(),partieIPA(),poid(0){
		// On réserve de la place pour 10 successeurs, au dela il faudra
		// faire une réalocation à chaque ajout d'un successeur
		listeSuccesseur.reserve(10);
		cout << this << " - Creation" << endl;
	}

protected:
	/**
	 * @brief Constructeur de la classe motif
	 * @details Ce constructeur ne doit en aucun cas être appelé par
	 *          l'utilisateur de la classe. Il est appelé par la mé-
	 *          thode ajouterSuccesseur().
	 * 
	 * @param ptr_predecesseur [description]
	 */
	Motif(Motif* ptr_predecesseur):listeSuccesseur(0),predecesseur(ptr_predecesseur),partieFR(),partieIPA(),poid(0){
		// On réserve de la place pour 10 successeurs, au dela il faudra
		// faire une réalocation à chaque ajout d'un successeur
		listeSuccesseur.reserve(10);
		cout << this << " - Creation" << endl;
	}

public:
	/**
	 * @brief Destructeur de la classe Motif
	 * @details Le destructeur est chargé de détruire les successeurs
	 *          du Motif.
	 */
	~Motif(){
		for(vector<Motif*>::iterator it = listeSuccesseur.begin(); it != listeSuccesseur.end(); ++it){
			Motif* &successeur = *it;
			delete successeur;
		}
		cout << this << " - Destruction" << endl;
	}

	/**
	 * @brief Fonction d'affichage d'un arbre
	 * @details Permet d'afficher tous les Motifs qui composent un abre
	 */
	void afficher(int indice = 0){
		cout << this << " - Motif - " << partieFR << "/" << partieIPA << endl;
		for(vector<Motif*>::iterator it = listeSuccesseur.begin(); it != listeSuccesseur.end(); ++it){
			Motif* &successeur = *it;
			for(int i = 0; i <= indice; i++){
				cout << "\t - ";
			}
			successeur->afficher(indice+1);
		}
	}

	/**
	 * @brief Ajoute un successeur au motif
	 * @details Crée un nouveau Motif successeur du Motif sur lequel
	 *          est appliqué la méthode.
	 */
	Motif* ajouterSuccesseur(){
		listeSuccesseur.push_back(NULL);
		listeSuccesseur.back() = new Motif(this);
		return listeSuccesseur.back();
	}

	/**
	 * @brief Permet d'accéder au successeur numéro n
	 * 
	 * @param n Le numéro du successeur auquel accéder
	 * @return Un pointeur sur le successeur 
	 */
	Motif* get_successeur(int n){
		return listeSuccesseur.at(n);
	}

	/**
	 * @brief Permet d'accéder au conteneur des successeurs
	 * @return Un pointeur sur le conteneur des successeurs
	 */
	vector<Motif*>* get_listeSuccesseur(){
		return &listeSuccesseur;
	}

	/**
	 * @brief Permet d'accéder à la partie FR du motif
	 * @return La partie FR du motif
	 */
	string get_partieFR(){
		return partieFR;
	}

	/**
	 * @brief Permet d'accéder à la partie IPA du motif
	 * @return La partie IPA du motif
	 */
	string get_partieIPA(){
		return partieIPA;
	}

	/**
	 * @brief Permet d'accéder au poid du motif
	 * @return Le poid du motif
	 */
	double get_poid(){
		return poid;
	}

	/**
	 * @brief Permet de définir la partie FR du motif
	 */
	void set_partieFR(string FR){
		partieFR = FR;
	}

	/**
	 * @brief Permet de définir la partie IPA du motif
	 */
	void set_partieIPA(string IPA){
		partieIPA = IPA;
	}

	/**
	 * @brief Permet de définir le poid du motif
	 */
	void set_poid(double p){
		poid = p;
	}
};